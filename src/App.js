import "./App.css";
import GlassesShop from "./ExerciseGlasses/GlassesShop";

function App() {
  return (
    <div className="App">
      <GlassesShop />
    </div>
  );
}

export default App;
