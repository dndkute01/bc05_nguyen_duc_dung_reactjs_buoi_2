import React, { Component } from "react";
import ListGlasses from "./ListGlasses";
import Model from "./Model";
import { dataGlasses } from "../ExerciseGlasses/dataGlasses";

export default class GlassesShop extends Component {
  state = {
    img: "./glassesImage/v1.png",
    name: "GUCCI G8850U",
    price: "30",
  };
  handleGlass = (img, name, price) => {
    this.setState({
      img: img,
      name: name,
      price: price,
    });
  };
  renderListGlasses = () => {
    return dataGlasses.map((item) => {
      return <ListGlasses handleGlass={this.handleGlass} data={item} />;
    });
  };
  render() {
    let { img, name, price } = this.state;
    return (
      <div>
        <div
          className="wrapper"
          style={{
            backgroundImage: "url(./glassesImage/background.jpg)",
            backgroundSize: "cover",
            backgroundRepeat: "no-repeat",
          }}
        >
          <h1 className="pt-4 title">Fashion Glasses Shop</h1>
          <div className="container-fluid ">
            <div className="row pt-4">
              <div className="col-4">
                <Model img={img} name={name} price={price} />
              </div>
              <div className="col-8">
                <div className="row">{this.renderListGlasses()}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
