import React, { Component } from "react";
import "../../src/index.css";

export default class Model extends Component {
  render() {
    let { img, name, price } = this.props;
    return (
      <div>
        <div className="card text-left">
          <img
            className="card-img-top model_img"
            src="./glassesImage/model.jpg"
            alt
          />
          <img className="glass_img" src={img} alt="" />
          <div className="card-body bg-dark text-center text-white">
            <h4 className="card-title">{name}</h4>
            <p className="card-text">Price:{price}$</p>
          </div>
        </div>
      </div>
    );
  }
}
